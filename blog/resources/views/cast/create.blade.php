@extends('adminLTE.master')

@section('content')
    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Membuat Data Pemain Film Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/cast" method="POST">
                  @csrf

            <!-- Nama -->
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama Cast</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{old ('nama', '')}}" placeholder="Masukkan Nama Cast" required>
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

            <!-- Umur -->
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" value="{{old ('umur', '')}}" placeholder="Masukkan Umur Cast" required>
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                  </div>

            <!-- Bio-->
                  <div class="form-group">
                    <label for="bio">Biodata Cast</label>
                    <textarea class="form-control" rows="3" id="bio" name="bio" value="{{old ('bio', '')}}" placeholder="Enter ..." required></textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                  </div>

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

@endsection
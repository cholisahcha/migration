<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKritikTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kritik', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->integer('film_id')->unsigned();
            //$table->integer('user_id')->unsigned();
            //$table->foreign('user_id')->references('id')->on('user');
            //$table->unsignedBigInteger('film_id');
            //$table->foreign('film_id')->references('id')->on('film');
            $table->text('isi');
            $table->integer('point');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kritik', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropColumn('user_id');
            $table->dropColumn('film_id');
            $table->dropColumn('isi');
            $table->dropColumn('point');
        });
    }
}
